import os
from unittest import IsolatedAsyncioTestCase

import matplotlib.pyplot as plt
import pandas as pd
from dotenv import load_dotenv

from dex_adaptors.aave_v3 import AaveV3
from dex_adaptors.balancer_v2 import BalancerV2
from dex_adaptors.compound_v2 import CompoundV2
from dex_adaptors.curve import Curve
from dex_adaptors.morpho import Morpho
from dex_adaptors.pendle import Pendle
from dex_adaptors.silo import Silo
from dex_adaptors.uniswap_v3 import UniswapV3

load_dotenv()

"""
Update package version process:
1. update setup.py version
2. run `python setup.py sdist bdist_wheel`
3. run `twine upload dist/*`
"""


class TestSilo(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        self.exchange = Silo(os.getenv("SUBGRAPH_API_KEY"))

    async def asyncTearDown(self):
        pass

    async def test_get_pool_data(self):
        address = "0xCD7ae3373F7e76A817238261b8303FA17D2AF585"
        data = await self.exchange.get_pool_data(address)
        self.assertTrue(data)
        return


class TestUniswapV3(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        self.exchange = UniswapV3()

    async def asyncTearDown(self):
        pass

    async def test_get_pool_data(self):
        ezETH_WETH_pool_address = "0xbe80225f09645f172b079394312220637c440a63"
        data = await self.exchange.get_pool_data(ezETH_WETH_pool_address)
        self.assertTrue(data)

        return data

    async def test_get_pool_candlesticks(self):
        ezETH_WETH_pool_address = "0xbe80225f09645f172b079394312220637c440a63"
        num = 1000
        data = await self.exchange.get_pool_token_candlesticks(ezETH_WETH_pool_address, num)
        self.assertTrue(data)

        return data

    async def test_get_pool_price_canlesticks(self):
        ezETH_WETH_pool_address = "0xbe80225f09645f172b079394312220637c440a63"
        num = 1000
        data = await self.exchange.get_pool_price_candlesticks(ezETH_WETH_pool_address, num)
        self.assertTrue(data)

        return data


class TestPendle(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        self.exchange = Pendle()

    async def asyncTearDown(self):
        await self.exchange.close()

    async def test_get_market_by_address(self):
        chain = "ethereum"
        address = "0xd8f12bcde578c653014f27379a6114f67f0e445f"
        data = await self.exchange.get_market_by_address(chain, address)
        self.assertTrue(data)
        return

    async def test_get_exchange_info(self):
        chain = "ethereum"
        data = await self.exchange.get_exchange_info(chain)
        self.assertTrue(data)
        return


class TestMorpho(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        self.exchange = Morpho()

    async def asyncTearDown(self):
        pass

    async def test_get_vault_by_address(self):
        address = "0x73e65DBD630f90604062f6E02fAb9138e713edD9"
        chain_id = "ethereum"
        data = await self.exchange.get_vault_by_address(chain_id, address)
        self.assertTrue(data)
        return

    async def test_get_exchange_info(self):
        data = await self.exchange.get_exchange_info()
        self.assertTrue(data)
        return

    async def test_get_user_position(self):
        await self.exchange.get_exchange_info()
        user = "0x051ef87c542ec5ca717aa19210b2657598a57319"
        data = await self.exchange.get_user_positions(user)
        self.assertTrue(data)
        return

    async def test_get_market_positions(self):
        await self.exchange.get_exchange_info()
        unique_key = "0x698fe98247a40c5771537b5786b2f3f9d78eb487b4ce4d75533cd0e94d88a115"
        data = await self.exchange.get_market_positions(unique_key)
        self.assertTrue(data)
        return

    async def test_get_market_liquidation_graph(self):
        """
        How to turn ltv into liquidation price when the pool asset is in 1:1 ratio in ideal case?
        Liquidation price = borrow amount / collateral amount * liquidation ltv
        """
        await self.exchange.get_exchange_info()
        unique_key = "0x698fe98247a40c5771537b5786b2f3f9d78eb487b4ce4d75533cd0e94d88a115"
        positions = await self.exchange.get_market_positions(unique_key)

        for i in range(len(positions)):
            position = positions[i]
            liquidation_price = position["borrow"]["usd_value"] / position["collateral"]["usd_value"] * position["lltv"]
            positions[i]["liquidation_price"] = liquidation_price
            print(
                f"Pool: {position['pool_name']}, Liquidation Price: {liquidation_price} Borrow USD: {position['borrow']['usd_value']} Collateral USD: {position['collateral']['usd_value']}"
            )

        """
        Draw the liquidation chart,
        use liquidation price as x and liquidation borrow asset USD as y (use cumulative sum)
        """
        datas = pd.DataFrame(positions)[["liquidation_price", "collateral"]].sort_values(
            by="liquidation_price", ascending=False
        )
        datas["collateral_value"] = datas["collateral"].apply(lambda x: x["usd_value"])

        fig, ax = plt.subplots()
        ax.scatter(datas["liquidation_price"], datas["collateral_value"].cumsum())
        plt.show()
        return


class TestAaveV3(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        self.exchange = AaveV3()

    async def asyncTearDown(self):
        pass

    async def test_get_borrow_rates(self):
        num = 1000
        data = await self.exchange.get_borrow_rates(num)
        self.assertTrue(data)
        return


class TestCompoundV2(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        self.exchange = CompoundV2()

    async def asyncTearDown(self):
        pass

    async def test_get_markets_data(self):
        data = await self.exchange.get_markets()
        self.assertTrue(data)
        return


class TestCurve(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        self.exchange = Curve()

    async def asyncTearDown(self):
        await self.exchange.close()

    async def test_get_pools_data(self):
        chain = "ethereum"
        data = await self.exchange.get_pools_data(chain)
        self.assertTrue(data)
        return


class TestBalancerV2(IsolatedAsyncioTestCase):
    async def asyncSetUp(self):
        self.exchange = BalancerV2()

    async def asyncTearDown(self):
        pass

    async def test_get_pool_data(self):
        pool_id = "0x596192bb6e41802428ac943d2f1476c1af25cc0e000000000000000000000659"
        data = await self.exchange.get_pool_data(pool_id)
        self.assertTrue(data)
        return


class TestMetaData(IsolatedAsyncioTestCase):
    UNI_POOL_MAP = {
        "LRT/ETH": {
            "ezETH/ETH": {"address": "0xBE80225f09645f172B079394312220637C440A63"},
            "weETH/ETH": {"address": "0x7A415B19932c0105c82FDB6b720bb01B0CC2CAe3"},
            "rsETH/ETH": {"address": "0x48b0aB72c2591849e678e7d6f272b75eF9b863F7"},
            "pufETH/ETH": {"address": "0xBDB04e915B94FbFD6e8552ff7860E59Db7d4499a"},
        },
        "LST/ETH": {
            "wstETH/ETH": {"address": "0x109830a1AAaD605BbF02a9dFA7B0B92EC2FB7dAa"},
            "stETH/ETH": {"address": "0x4028DAAC072e492d34a3Afdbef0ba7e35D8b55C4"},
        },
    }

    CUR_POOL_MAP = {
        "LRT/ETH": {
            "weETH/WETH": {"address": "0x13947303f63b363876868d070f14dc865c36463b"},
            "ezETH/WETH": {"address": "0x85de3add465a219ee25e04d22c39ab027cf5c12e"},
            "pufETH/WETH": {"address": "0x39f5b252de249790faed0c2f05abead56d2088e1"},
        },
        "LST/ETH": {"stETH/ETH": {"address": "0xdc24316b9ae028f1497c275eb9192a3ea0f67022"}},
        "LRT/LST": {"pufETH/wstETH": {"address": "0xeeda34a377dd0ca676b9511ee1324974fa8d980d"}},
    }

    BAL_POOL_MAP = {}

    async def asyncSetUp(self):
        self.uni = UniswapV3()
        self.bal = BalancerV2()
        self.cur = Curve()

    async def asyncTearDown(self):
        await self.cur.close()

    async def test_get_currency_liquidity(self):
        curve_pools_data = await self.cur.get_pools_data("ethereum")

        for cat in self.CUR_POOL_MAP:
            for pool in self.CUR_POOL_MAP[cat]:
                print(f"Getting data for {pool} pool from Curve")
                pool_address = self.CUR_POOL_MAP[cat][pool]["address"]
                token_list = pool.split("/")
                data = [i for i in curve_pools_data.values() if i["address"] == pool_address][0]

                base = [i for i in data["currency"] if i["symbol"] == token_list[0]][0]
                quote = [i for i in data["currency"] if i["symbol"] == token_list[1]][0]
                self.CUR_POOL_MAP[cat][pool]["data"] = {
                    "base_qty": base["liquidity"],
                    "quote_qty": quote["liquidity"],
                    "base_usd": base["price_usd"] * base["liquidity"],
                    "quote_usd": quote["price_usd"] * quote["liquidity"],
                    "pool_usd": data["pool_usd"],
                }
                continue

        for cat in self.UNI_POOL_MAP:
            for pool in self.UNI_POOL_MAP[cat]:
                print(f"Getting data for {pool} pool")
                pool_address = self.UNI_POOL_MAP[cat][pool]["address"]
                self.UNI_POOL_MAP[cat][pool]["data"] = await self.uni.get_pool_data(pool_address)
                continue

    async def test_get_apy(self):
        aave = AaveV3()
        compound = CompoundV2()

        aave_borrow_rates = await aave.get_borrow_rates(1000)
        compound_markets = await compound.get_markets(1000)

        target_currency_list = {
            "WBTC": ["WBTC", "BTC"],
            "WETH": ["WETH", "ETH"],
            "USDT": ["USDT"],
            "USDC": ["USDC"],
            "DAI": ["DAI"],
        }

        results = pd.DataFrame(columns=["protocol", "currency", "borrow_rate", "supply_rate"])

        for currency in target_currency_list:
            for i in target_currency_list[currency]:
                if i in aave_borrow_rates:
                    results.loc[len(results)] = ["aave", currency, aave_borrow_rates[i]["variable_borrow_rate"], None]
                if i in compound_markets:
                    results.loc[len(results)] = [
                        "compound",
                        currency,
                        compound_markets[i]["borrow_rate"],
                        compound_markets[i]["supply_rate"],
                    ]
        return
