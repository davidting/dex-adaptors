from .exchange.etherscan import EtherscanUnified
from .parsers.etherscan import EtherscanParser


class Etherscan(EtherscanUnified, EtherscanParser):
    def __init__(self, etherscan_api_key: str):
        super().__init__(etherscan_api_key=etherscan_api_key)

    async def get_token_total_supply(self, token_address: str) -> dict:
        return self.parse_token_total_supply(
            await self._get_token_total_supply(token_address=token_address), token_address
        )
