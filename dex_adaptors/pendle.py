from .exchange.pendle import PendleUnified
from .parsers.pendle import PendleParser


class Pendle(PendleUnified, PendleParser):
    async def get_exchange_info(self, chain: str) -> dict:
        if chain not in self.CHAIN_ID_MAP.keys():
            raise ValueError(f"Chain {chain} not supported. Supported chains: {self.CHAIN_ID_MAP.keys()}")
        chain_id = self.CHAIN_ID_MAP[chain]

        params = {"chain_id": chain_id, "limit": 100, "is_active": True, "select": "all"}
        return self.parse_exchange_info(await self._get_markets(**params))

    async def get_market_by_address(self, chain: str, address: str) -> dict:
        if chain not in self.CHAIN_ID_MAP.keys():
            raise ValueError(f"Chain {chain} not supported. Supported chains: {self.CHAIN_ID_MAP.keys()}")
        chain_id = self.CHAIN_ID_MAP[chain]

        params = {"chain_id": chain_id, "address": address}
        return self.parse_market_by_address(await self._get_market_by_address(**params))

    async def get_history_apy(
        self, chain: str, address: str, interval: str, start: int = None, end: int = None
    ) -> list:
        if chain not in self.CHAIN_ID_MAP.keys():
            raise ValueError(f"Chain {chain} not supported. Supported chains: {self.CHAIN_ID_MAP.keys()}")
        chain_id = self.CHAIN_ID_MAP[chain]

        params = {
            k: v
            for k, v in {
                "time_frame": interval,
                "timestamp_start": self.get_iso_time(start),
                "timestamp_end": self.get_iso_time(end),
                "address": address,
                "chain_id": chain_id,
            }.items()
            if v
        }
        return self.parse_history_apy(await self._get_market_history_apy(**params))
