from .base import HttpsBase


class PendleUnified(HttpsBase):
    BASE_ENDPOINT = "https://api-v2.pendle.finance/core"

    def __init__(self):
        super().__init__(base_endpoint=self.BASE_ENDPOINT)

    async def _get_market_by_address(self, chain_id: str, address: str) -> dict:
        url = f"/v1/{chain_id}/markets/{address}"
        return await self.get(url=url)

    async def _get_market_data_by_address(self, chain_id: str, address: str) -> dict:
        url = f"/v2/{chain_id}/markets/{address}/data"
        return await self.get(url=url)

    async def _get_market_history_apy(
        self,
        chain_id: str,
        address: str,
        time_frame: str = "day",
        timestamp_start: str = None,
        timestamp_end: str = None,
    ) -> dict:
        url = f"/v1/{chain_id}/markets/{address}/apy-history"
        params = {
            k: v
            for k, v in {
                "time_frame": time_frame,
                "timestamp_start": timestamp_start,
                "timestamp_end": timestamp_end,
            }.items()
            if v
        }
        return await self.get(url=url, params=params)

    async def _get_markets(
        self,
        chain_id: str,
        order_by: str = None,
        skip: int = None,
        limit: int = None,
        is_expired: bool = None,
        select: str = None,
        pt: str = None,
        yt: str = None,
        sy: str = None,
        q: str = None,
        is_active: bool = None,
        categoryId: str = None,
    ):
        url = f"/v1/{chain_id}/markets"
        params = {
            k: v
            for k, v in {
                "orderBy": order_by,
                "skip": skip,
                "limit": limit,
                "isExpired": is_expired,
                "select": select,
                "pt": pt,
                "yt": yt,
                "sy": sy,
                "q": q,
                "isActive": is_active,
                "categoryId": categoryId,
            }.items()
            if v
        }
        return await self.get(url=url, params=params)
