from .base import HttpsBase


class EtherscanUnified(HttpsBase):
    def __init__(self, etherscan_api_key: str):
        super().__init__(base_endpoint="https://api.etherscan.io/api")
        self.etherscan_api_key = etherscan_api_key

    async def _get_token_total_supply(self, token_address: str) -> dict:
        params = {
            "module": "stats",
            "action": "tokensupply",
            "contractaddress": token_address,
            "apikey": self.etherscan_api_key,
        }
        return await self.get(url="", params=params)
