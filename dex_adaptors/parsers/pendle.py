from datetime import datetime as dt
from urllib.parse import quote

from .base import Parser


class PendleParser(Parser):
    def __init__(self):
        super().__init__(name="pendle")

    def check_response(self, response: dict) -> dict:
        return response

    def parse_market_by_address(self, response: dict) -> dict:
        response = self.check_response(response)
        datas = response

        pt = datas["pt"]
        yt = datas["yt"]
        return {
            "timestamp": self.get_timestamp(),  # current timestamp
            "active": datas["isActive"],  # "isActive"
            "chain": {v: k for k, v in self.CHAIN_ID_MAP.items()}[datas["chainId"]],  # "chainId"
            "instrument_id": self.parse_instrument_id(datas),  # "proSymbol" and "expiry"
            "address": datas["address"],
            "maturity_date": self.parse_iso_timestamp(datas["expiry"]),
            "pt_token": {
                "name": pt["symbol"],
                "address": pt["address"],
                "price": pt["price"]["acc"],
                "usd_price": pt["price"]["usd"],
            },
            "yt_token": {
                "name": yt["symbol"],
                "address": yt["address"],
                "price": yt["price"]["acc"],
                "usd_price": yt["price"]["usd"],
            },
            "implied_apy": self.parse_str(datas["impliedApy"], float),
            "underlying_apy": self.parse_str(datas["underlyingApy"], float),
            "updated_timestamp": self.parse_iso_timestamp(datas["dataUpdatedAt"]),
            "raw_data": datas,
        }

    def parse_instrument_id(self, datas: dict) -> str:
        iso_timestamp = datas["expiry"].replace("Z", "+00:00")
        expiry = dt.fromisoformat(iso_timestamp).strftime("%y%m%d")

        symbol = datas["proSymbol"]
        return f"{symbol}-{expiry}"

    def parse_iso_timestamp(self, time_string: str) -> int:
        return int(dt.fromisoformat(time_string.replace("Z", "+00:00")).timestamp() * 1000)

    def parse_history_apy(self, response: dict) -> list:
        response = self.check_response(response)
        datas = response["results"]

        return [
            {
                "timestamp": self.parse_iso_timestamp(data["timestamp"]),
                "implied_apy": self.parse_str(data["impliedApy"], float),
                "underlying_apy": self.parse_str(data["underlyingApy"], float),
                "raw_data": data,
            }
            for data in datas
        ]

    def get_iso_time(self, timestamp: int) -> str:
        if not timestamp:
            return ""
        time_string = dt.fromtimestamp(timestamp / 1000).isoformat()
        return f"{quote(time_string)}.000Z"

    def parse_exchange_info(self, response: dict) -> dict:
        response = self.check_response(response)
        datas = response["results"]

        results = {}
        chain = {v: k for k, v in self.CHAIN_ID_MAP.items()}[datas[0]["chainId"]]
        for data in datas:
            if not data["isActive"] or data["proSymbol"] == "Deprecated":
                continue
            pt = data["pt"]
            yt = data["yt"]
            instrument_id = self.parse_instrument_id(data)

            results[instrument_id] = {
                "active": data["isActive"],
                "chain": chain,
                "instrument_id": instrument_id,
                "address": data["address"],
                "maturity_date": self.parse_iso_timestamp(data["expiry"]),
                "pt_token": {
                    "name": pt["symbol"],
                    "address": pt["address"],
                    "price": pt["price"]["acc"] if "price" in pt else None,
                    "usd_price": pt["price"]["usd"] if "price" in pt else None,
                },
                "yt_token": {
                    "name": yt["symbol"],
                    "address": yt["address"],
                    "price": yt["price"]["acc"] if "price" in yt else None,
                    "usd_price": yt["price"]["usd"] if "price" in yt else None,
                },
                "implied_apy": self.parse_str(data["impliedApy"], float) if "impliedApy" in data else None,
                "underlying_apy": self.parse_str(data["underlyingApy"], float) if "underlyingApy" in data else None,
                "updated_timestamp": self.parse_iso_timestamp(data["dataUpdatedAt"])
                if "dataUpdatedAt" in data
                else None,
                "raw_data": data,
            }
        return results
