from .base import Parser


class EtherscanParser(Parser):
    default_token_decimals = 18

    def __init__(self):
        super().__init__(name="etherscan")

    def parse_token_total_supply(self, response: dict, address: str) -> int:
        data = response
        return {"address": address, "total_supply": float(data.get("result", 0)) / 10**self.default_token_decimals}
